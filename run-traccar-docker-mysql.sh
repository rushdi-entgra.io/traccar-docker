sudo docker run \
-d --restart always \
--name traccar \
--hostname traccar \
-p 8090:8082 \
-p 5000-5150:5000-5150 \
-p 5000-5150:5000-5150/udp \
-v /var/docker/traccar/data/mysql:/opt/traccar/data:rw \
-v /var/docker/traccar/logs:/opt/traccar/logs:rw \
-v /var/docker/traccar/confs/traccar-mysql.xml:/opt/traccar/conf/traccar.xml:ro entgra/traccar:4.15
